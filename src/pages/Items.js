import { Fragment, useEffect, useState } from 'react'
import ItemCard from '../components/ItemCard';

export default function Items() {

	const [items, setCourses] = useState([])


	//console.log(coursesData[0])
	// const courses = coursesData.map(course => {
	// 	return(
	// 		<CourseCard key={course.id} courseProp={course} />
	// 		)
	// })
	useEffect(() => {
		fetch("https://rocky-reaches-97141.herokuapp.com/api/items/allItems/")
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setCourses(data.map(items => {
				return(
					<ItemCard key={items._id} itemProp={items} />			
					)
			}))
		})
	}, [])

	return(
		<Fragment>
			<h1>Products</h1>
			{items}
		</Fragment>	
		)
}

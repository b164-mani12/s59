import { useState, useEffect, useContext } from 'react';
import { Form, Button} from 'react-bootstrap';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { Navigate } from 'react-router-dom';


export default function Login() {

		const { user, setUser } = useContext(UserContext);

		// State hooks to store the values of the input fields
		const [username, setUsername] = useState("");
	    const [password, setPassword] = useState("");
	    // State to determine whether submit button is enabled or not
	    const [isActive, setIsActive] = useState(true);


	function authenticate(e) {

		e.preventDefault()

		fetch('https://rocky-reaches-97141.herokuapp.com/api/users/login', {
			method : 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				username: username,
				password: password
			})	
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data.accessToken)

			if(typeof data.accessToken !== "undefined") {
				localStorage.setItem('token', data.accessToken)
				retrieveUserDetails(data.accessToken)
				
				Swal.fire({
					title:"Login Succesful",
					icon: "success",
					text: "Happy Shopping"
				});
			} else {
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your login details and try again!"
				})
			}
		})

		setUsername("");
		setPassword("");
		

	}

	const retrieveUserDetails = (token) => {
		
		fetch ('https://rocky-reaches-97141.herokuapp.com/api/users/userDetails', {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}


	useEffect(()=> {
		//Validation to enable the submit button when all the input fields are populated and both passwords match 
		if(username !== "" && password !== "") {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [username, password])

	return(

		(user.id !== null) ?

		<Navigate to="/items" />

		:

		<Form onSubmit={(e) => authenticate(e)}>
		<h1 className="text-center">Login</h1>
		  <Form.Group className="mb-3" controlId="setUsername">
		    <Form.Label>Username</Form.Label>
		    <Form.Control 
		    	type="username" 
		    	placeholder="Enter Username"
		    	value={username}
		    	onChange={e =>setUsername(e.target.value)} 
		    	required
		    />
		  </Form.Group>

		  <Form.Group className="mb-3" controlId="password">
		    <Form.Label>Password</Form.Label>
		    <Form.Control 
		    	type="password" 
		    	placeholder="Password" 
		    	value={password}
		    	onChange={e => setPassword(e.target.value)}
		    	required
		    />
		  </Form.Group>
		
		  {
		  	isActive ?
		  	<Button variant="success" type="submit" id="submitBtn">
		  	  Submit
		  	</Button>
		  	:
		  	<Button variant="danger" type="submit" id="submitBtn" disabled>
		  	  Submit
		  	</Button>

		  }
		</Form>

		)
}
import {Row, Col, Button} from 'react-bootstrap';

export default function Banner() {

	return(

		<Row>
			<Col className="p-5">
				<h1>REJ Trading Company</h1>
				<p>Together!, We build dreams.</p>
				<Button variant="primary">Shop Now</Button>
			</Col>
		</Row>

		)

}